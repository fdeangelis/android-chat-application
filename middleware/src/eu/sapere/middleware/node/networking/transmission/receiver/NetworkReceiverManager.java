package eu.sapere.middleware.node.networking.transmission.receiver;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;

import eu.sapere.middleware.agent.BasicSapereAgent;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.values.PropertyName;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.networking.transmission.protocols.tcpip.LsaReceived;
import eu.sapere.middleware.node.networking.transmission.protocols.tcpip.Server;
import eu.sapere.util.ISystemConfiguration;

/**
 * Provides the tcp-ip implementation for the receiver interface of the Sapere
 * networking.
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public class NetworkReceiverManager implements INetworkReceiverManager,
		LsaReceived {

	// a new ignore list of sent lsas
	private static HashMap<UUID,Integer> ignoreLsaIDs = new HashMap<UUID,Integer>();
	private BasicSapereAgent agent = null;

	/**
	 * Instantiates the NetworkReceiverManager.
	 */
	public NetworkReceiverManager() {

		new Server(
				new Integer(NodeManager.getSystemConfiguration()
						.getProperties()
						.getProperty(ISystemConfiguration.NETWORK_PORT)), this);
	}

	public void doInject(Lsa receivedLsa) {

		agent = new BasicSapereAgent("networkReceiverManager");

		agent.injectOperation(receivedLsa);

	}

	public void onLsaReceived(Lsa lsaReceived) {
		if (lsaReceived.getProperty("uuid") != null)
		{
			String stringUUID = lsaReceived.getProperty("uuid").getValue().get(0);
			if (!ignoreLsaIDs.containsKey(UUID.fromString(stringUUID)))
			{				
				String varName = lsaReceived.getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0);				
				String sHops = lsaReceived.getProperty(varName).getValue().get(0);
				int hops = Integer.parseInt(sHops);				
				ignoreLsaIDs.put(UUID.fromString(stringUUID), hops);
				doInject(lsaReceived);
			}
			else
			{
				Lsa lsa=null;	
				lsa = NodeManager.instance().getSpace().getLsaById(UUID.fromString(stringUUID));
				int hopsLocal= ignoreLsaIDs.get(UUID.fromString(stringUUID));
				String varName = lsaReceived.getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0);				
				
				String sHops = lsaReceived.getProperty(varName).getValue().get(0);
				int hopsRemote = Integer.parseInt(sHops);
				
				if (hopsLocal < hopsRemote && lsa != null)
				{
					
					lsa.addProperty(new Property(PropertyName.DIFFUSION_OP.toString(), "GRADIENT_" + hopsRemote));
					//update other property
					varName = lsaReceived.getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0);
					lsa.addProperty(new Property(varName,""+hopsRemote));	
				}
				else if (hopsLocal < hopsRemote && lsa == null)
				{
					doInject(lsaReceived);
				}
			}	
		}
		else
			doInject(lsaReceived);
	}

	public static HashMap<UUID,Integer> getIgnoreList()
	{
		return ignoreLsaIDs;
	}
	
}
