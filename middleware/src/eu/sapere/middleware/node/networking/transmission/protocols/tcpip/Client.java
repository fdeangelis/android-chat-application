package eu.sapere.middleware.node.networking.transmission.protocols.tcpip;

import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Date;
import java.util.HashMap;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.values.NeighbourLsa;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.util.ISystemConfiguration;

/**
 * The tcp/ip client for the Sapere network interface.
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public class Client {

	private ConnectionManager cManager;
	Lsa deliverLsa = null;
	Lsa remoteNode = null;
	public static long totalTime=0;
	public static int nMex=0;
	private String port = NodeManager.getSystemConfiguration().getProperties()
			.getProperty(ISystemConfiguration.NETWORK_PORT); // to be set in the
																// conf

	public Client()
	{
		cManager = ConnectionManager.getInstance();
	}
	/**
	 * Delivers a Lsa to the node specified.
	 * 
	 * @param deliverLsa
	 *            The Lsa to be delivered.
	 * @param destinationLsa
	 *            The Lsa that describes the destination node.
	 */
	public void deliver(Lsa deliverLsa, Lsa destinationLsa) {
		this.deliverLsa = deliverLsa;

		try 
		{
			String ip = destinationLsa
					.getProperty(NeighbourLsa.IP_ADDRESS.toString()).getValue()
					.elementAt(0);
			// PERFORMANCE
			long start= System.currentTimeMillis();
			
			// get the associated socket if it's existing
			//Socket socket = cManager.getSocketConnection(ip, new Integer(port));
			//Socket socket = new Socket(ip,new Integer(port));
			//ObjectOutputStream oos = new ObjectOutputStream(
			//	socket.getOutputStream());
			//oos.writeObject(deliverLsa);	
			//socket.close();
			
			PoolThread.getInstance().pushLsa(deliverLsa, ip);
			totalTime += System.currentTimeMillis()-start;
			nMex++;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
