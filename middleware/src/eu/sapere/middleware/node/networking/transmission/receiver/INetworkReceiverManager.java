package eu.sapere.middleware.node.networking.transmission.receiver;

import eu.sapere.middleware.lsa.Lsa;

/**
 * Provides an interface for the receiver interface of the Sapere networking.
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public interface INetworkReceiverManager {

	/**
	 * Injects a Lsa received by the network Sapere interface in the local Lsa
	 * space.
	 * 
	 * @param receivedLsa
	 *            The rceived Lsa.
	 */
	public void doInject(Lsa receivedLsa);

}
