package eu.sapere.middleware.node.networking.transmission.delivery;

import eu.sapere.middleware.lsa.Lsa;

/**
 * Provides an interface for the delivery interface of the Sapere networking.
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public interface INetworkDeliveryManager {

	/**
	 * Delivers a Lsa from the local Lsa space to a neighbour Sapere node.
	 * 
	 * @param deliverLsa
	 *            The Lsa to be delivered.
	 * @param destinationLsa
	 *            The Lsa that describes the destination node.
	 * @return true if the deliverLsa has been delivered, false otherwise.
	 * 
	 */
	public boolean doSpread(Lsa deliverLsa, Lsa destinationLsa);

}
