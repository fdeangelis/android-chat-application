package tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import sapere.Global;





public class ConfigReader 
{
	private String [][] neighs;
	private String nodeName;
	private String nodeIp;
	private String node="A";
	public String error;
	
	public ConfigReader()
	{
		try
		{
			InputStream fis = new FileInputStream(new File(Global.CONFIG_FILE));
			BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
			node = reader.readLine();
			reader.close();
			fis.close();
			error = "NULL";
		}
		catch(Exception ex)
		{
			error = ex.getMessage();
		}
		
		if (node.equals("A"))
		{
			nodeName = "Tablet1";
			nodeIp = "192.168.1.11";
			neighs = new String[1][];
			neighs[0] = new String[2];
			neighs[0][0] = "192.168.1.12";
			neighs[0][1] = "Tablet2";
//			neighs[1] = new String[2];
//			neighs[1][0] = "192.168.1.15";
//			neighs[1][1] = "Tablet5";
		}
		else if (node.equals("B"))
		{
			nodeName = "Tablet2";
			nodeIp = "192.168.1.12";
			neighs = new String[2][];
			neighs[0] = new String[2];
			neighs[0][0] = "192.168.1.11";
			neighs[0][1] = "Tablet1";
			neighs[1] = new String[2];
			neighs[1][0] = "192.168.1.13";
			neighs[1][1] = "Tablet3";
		}
		else if (node.equals("C"))
		{
			nodeName = "Tablet3";
			nodeIp = "192.168.1.13";
			neighs = new String[2][];
			neighs[0] = new String[2];
			neighs[0][0] = "192.168.1.12";
			neighs[0][1] = "Tablet2";
			neighs[1] = new String[2];
			neighs[1][0] = "192.168.1.14";
			neighs[1][1] = "Tablet4";
		}
		else if (node.equals("D"))
		{
			nodeName = "Tablet4";
			nodeIp = "192.168.1.14";
			neighs = new String[1][];
			neighs[0] = new String[2];
			neighs[0][0] = "192.168.1.13";
			neighs[0][1] = "Tablet3";
//			neighs[1] = new String[2];
//			neighs[1][0] = "192.168.1.15";
//			neighs[1][1] = "Tablet5";
		}
		
//		else if (node.equals("E"))
//		{
//			nodeName = "Tablet5";
//			nodeIp = "192.168.1.15";
//			neighs = new String[2][];
//			neighs[0] = new String[2];
//			neighs[0][0] = "192.168.1.11";
//			neighs[0][1] = "Tablet1";
//			neighs[1] = new String[2];
//			neighs[1][0] = "192.168.1.14";
//			neighs[1][1] = "Tablet4";
//		}
	}
	
	public String getNodeName()
	{
		return nodeName;
	}
	
	public String getNodeIp()
	{
		return nodeIp;
	}	
	public String[][] getNeighs()
	{
		return neighs;
	}
}
