/*
 * A specific Network Analyzer used to specify the topology of the tablets of the network
 * 
 * GPL v.3
 * 
 * Francesco De Angelis
 * 
 * francesco.deangelis@unige.ch
 * 
 */

package sapere;

import android.util.Log;
import eu.sapere.middleware.node.networking.topology.overlay.Neighbour;
import eu.sapere.middleware.node.networking.topology.overlay.NeighbourProperty;
import eu.sapere.middleware.node.networking.topology.overlay.OverlayAnalyzer;
import eu.sapere.middleware.node.networking.topology.overlay.OverlayListener;

public class NetworkAnalyzer extends OverlayAnalyzer {

	public NetworkAnalyzer(OverlayListener listener, String overlayName) {
		super(listener, overlayName);
		// TODO Auto-generated constructor stub
		System.out.println("MyStatic Overlay Network initialized");
		Log.d("NetworkAnalyzer invocation","Invocato");
	}


	@Override
	public void run() {
		tools.AndroidConfigReader config = new tools.AndroidConfigReader();
		String [][] neighs = config.getNeighs();
		Neighbour n;
		for(int i = 0; i < neighs.length; i++)
		{
			n = new Neighbour(neighs[i][0], neighs[i][1], new NeighbourProperty("nodeType", "mobile"));
			addNeighbour(n);
		}
		//Neighbour n2 = new Neighbour("155.185.49.35", "gabriella", new NeighbourProperty("nodeType", "mobile"));
		//addNeighbour(n2);
	}


	@Override
	public void stopOverlay() {
		// TODO Auto-generated method stub
		
	}

}
