/*
 * Chemotaxis library specific for the chat. 
 * It implements the AgentChemo class.
 * 
 * GPL v.3
 * 
 * Francesco De Angelis
 * 
 * francesco.deangelis@unige.ch
 * 
 */
package sapere;

import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.DecayedEvent;
import eu.sapere.middleware.node.notifier.event.LsaUpdatedEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.event.ReadEvent;
import sapere.Global;

public class AgentChemo extends SapereAgent{
	private String value;
	private String destination;
	private String source;
	private String finalDestination=null;
	private String agentInjector;
	private String nodeName;
	private String gradientId;
	private String sendAgain;
	private int idMex;
	
	public AgentChemo(String value, String destination, 
			String source, String finalDestination, int idMex,
			String agentInjector, String nodeName, String gradientId,
			String sendAgain) {
		super("AgentChemoSender");		
		// the value to send back
		this.value = value;
		// the destination
		this.destination = destination;
		this.source = source;
		this.finalDestination = finalDestination;
		this.idMex = idMex;
		this.agentInjector = agentInjector;
		this.nodeName = nodeName;
		this.gradientId = gradientId; 
		this.sendAgain = sendAgain;
	} 
	 
	@Override
	public void setInitialLSA() {
		this.addProperty(new Property("MyName", "AgentChemoSender"));
		// add the index of the message
		this.addProperty(new Property(Global.CHEMO_PREVIOUS_SENDER,nodeName));
		this.addProperty(new Property(Global.CHEMO_CHEMO, "chemotaxis"));
		// destination of the chemo (1 hop)
		this.addProperty(new Property(Global.CHEMO_DESTINATION,destination));
		// final destination of the chemo (real recipient)
		this.addProperty(new Property(Global.CHEMO_FINAL_DESTINATION,finalDestination));
		// node passing back the chemo 
		this.addProperty(new Property(Global.CHEMO_SOURCE_CHEMO,source));
		// agent name when the chemo is generated
		this.addProperty(new Property(Global.CHEMO_INJECTOR,agentInjector));
		// send again value
		this.addProperty(new Property(Global.CHEMO_SEND_AGAIN,sendAgain));
		// chemo value
		this.addProperty(new Property(Global.CHEMO_VALUE, this.value));
		// gradient id
		this.addProperty(new Property(Global.GRADIENT_ID, this.gradientId));
		// id of the message
		this.addProperty(new Property(Global.MEX_ENC_ID, ""+this.idMex));
		// send it to the previous node
		this.addDirectPropagation(destination);
		this.addDecay(10);
		this.submitOperation();
	}

	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {	
	}
	
	@Override
	public void onPropagationEvent(PropagationEvent event) {
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) {
	}

	@Override
	public void onLsaUpdatedEvent(LsaUpdatedEvent event) {	
	}

	@Override
	public void onReadNotification(ReadEvent readEvent) {	
	}

}
