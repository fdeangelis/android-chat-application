package sapere;

import java.security.PublicKey;
import java.util.ArrayList;

import eu.sapere.android.SapereConsoleActivity;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.values.AggregationOperators;
import eu.sapere.middleware.lsa.values.PropertyName;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.DecayedEvent;
import eu.sapere.middleware.node.notifier.event.LsaUpdatedEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.event.ReadEvent;
import securitymanager.EncryptedMessage;
import securitymanager.KeyManager;
import tools.AndroidConfigReader;
import tools.PrinterDebug;
import android.content.Context;
import android.util.Log;
import org.apache.commons.codec.binary.Base64;

public class AgentGradient  extends SapereAgent{

	private String ip;
	private boolean received = false;
	private String nodeName;
	private int idGradient;
	private String value;
	// unique id
	private String gradientId;
	private String encodedPublicKey;
	private int id;
	
	public AgentGradient(String ip, String nodeName, int id, String gradientId,
			String value, String key) {
		super("AgentInjecter");		
		Log.d("AgentInjecter", "AgentInjecter Constructor");
		this.ip = ip;
		 this.nodeName = nodeName;
		 this.idGradient = id;
		 this.gradientId = gradientId;
		 this.value = value;
		 this.encodedPublicKey = key;
		 this.id = id;
	}  
	
	@Override
	public void setInitialLSA() {
		this.addProperty(new Property(Global.PUBLIC_KEY, encodedPublicKey));
		// add the sender of the message
		this.addProperty(new Property(Global.MEX_SENDER,this.nodeName));
		// id
		// add the unique gradient id
		this.addProperty(new Property(Global.GRADIENT_ID,this.gradientId));
		// gradient value
		this.addProperty(new Property(Global.GRADIENT_VALUE,this.value));
		// add gradient properties
		this.addGradient(Global.MAX_HOPS, "MAX", ""+this.idGradient);
		this.addProperty(new Property(Global.CHEMO_GRADIENT, ""+this.idGradient));
		this.addDecay(Global.DECAY);
		this.submitOperation();
	}

	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onPropagationEvent(PropagationEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onLsaUpdatedEvent(LsaUpdatedEvent event) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void onReadNotification(ReadEvent readEvent) {
		// TODO Auto-generated method stub	
	}

}
