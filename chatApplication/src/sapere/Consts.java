package sapere;

public class Consts 
{
	public static final String PROP_FROM_NAME = "FromAgentName";
	public static final String PROP_FROM_IP = "FromAgentIP";
	public static final String PROP_AGGREGATED_VALUE="x";
	public static final String PUBLIC_KEY = "publicKey";
	public static final String ENC_MESSAGE = "encryptedMessage";
	public static final String MEX_SENDER = "mexSender";
	public static final String CHEMO_GRADIENT = "chemoGradient";
	public static final String CHEMO_CHEMO = "chemoChemo";
	public static final String CHEMO_PREVIOUS = "previous";
	public static final String CHEMO_VALUE = "chemoValue";
	public static final String CHEMO_DESTINATION = "chemoDestination";
	public static final String CHEMO_FINAL_DESTINATION = "chemoFinalDestination";
	public static final String CHEMO_SOURCE_CHEMO= "chemoSource";
	public static final String GRADIENT_VALUE = "gradientValue";
	public static final String CHEMO_PREVIOUS_SENDER = "chemoPreviousSource";
	public static final String CHEMO_INJECTOR = "chemoAgentInjector";
	public static final String CHEMO_SEND_AGAIN = "ChemoSendAgain";
	public static final String AGENT_CHEMO_MANAGER = "AgentChemoManager";
	public static final String AGENT_CHEMO_SENDER = "AgentChemoSender";
	public static final String GRADIENT_ID = "GradientId";
	public static final String CONFIG_FILE="sdcard/sapere/config.txt";
	public static final String SF_KEY="key";
	public static final String SF_ENC="encrypted";
	public static final String SF_MEX="mex";
	public static final String SK_BLANK = "blank";
	public static final String NODE_SENDER1 = "TabletA";
	public static final String NODE_SENDER2 = "TabletE";
	public static final String NODE_RECEIVER1 = "TabletE";
	public static final String NODE_RECEIVER2 = "TabletA";
	//public static final String NODE_BRIDGE = "Tablet5";
	public static final String MEX_ENC_ID = "mexEncId";
	public static final String COLOR_CHANNEL_1 = "C1";
	public static final String COLOR_CHANNEL_2 = "C2";
	public static final int N_NODES = 5;
	public static final int	MAX_HOPS =  N_NODES;
	public static final int ARRAY_ASCII_START = 65;
	public static final int ARRAY_ASCII_END = 91;
	public static final int DECAY = 10;
	
	public static String [] stringMessages()
	{
		String[] result = new String[ARRAY_ASCII_END -ARRAY_ASCII_START ];
		for (int i = 0; i < ARRAY_ASCII_END -ARRAY_ASCII_START; i++)
		{
			int ch = (ARRAY_ASCII_START + i );
			result[i] = Character.toString((char) ch);
		}
		return result;
	}
}
