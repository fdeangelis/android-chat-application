package eu.sapere.android;

import securitymanager.KeyManager;
import tools.PrinterDebug;
import android.util.Base64;

public class Testing {

	
	static KeyManager key;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello world!!");
		try {
			key =  new KeyManager();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String encodedPublicKey;
		encodedPublicKey = new String(Base64.encode(key.getGeneratedPublicKey().getEncoded(),
				Base64.DEFAULT));
		long checksum=0;
		 for(int i = 0; i < encodedPublicKey.getBytes().length;i++ )
				checksum += encodedPublicKey.getBytes()[i];
		 
		System.out.println("Checksum before = " + checksum);

		
		String pKey = new String(Base64.decode(encodedPublicKey, Base64.DEFAULT));
		
		for(int i = 0; i < pKey.getBytes().length;i++ )
			checksum += pKey.getBytes()[i];
	   
		System.out.println("Checksum = " + checksum);
		
	}

}
