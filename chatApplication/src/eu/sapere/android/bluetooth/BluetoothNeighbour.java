package eu.sapere.android.bluetooth;

import eu.sapere.middleware.node.networking.topology.overlay.Neighbour;
import eu.sapere.middleware.node.networking.topology.overlay.NeighbourProperty;

/**
 * Provides an implementation of a bluetooth-featured Sapere Node neighbour.
 * 
 * @author Gabriella Castelli (UNIMORE)
 * @author Alberto Rosi (UNIMORE)
 */
public class BluetoothNeighbour extends Neighbour {

	/**
	 * Instantiates a bluetooth neighbour.
	 * 
	 * @param btMacAddress
	 *            The mac address of the bluetooth device
	 * @param ipAddress
	 *            The ip address of the device
	 * @param nodeName
	 *            The name of the Sapere node
	 * @param nodeType
	 *            The type of node
	 */
	public BluetoothNeighbour(String btMacAddress, String ipAddress,
			String rssi, String nodeName, String nodeType) {
		super(ipAddress, nodeName, new NeighbourProperty("rssi", nodeName),
				new NeighbourProperty("btMacAddress", nodeName),
				new NeighbourProperty("nodeType", nodeType));
	}

	/**
	 * Returns the mac address of the bluetooth device.
	 * 
	 * @return The mac address of the bluetooth device.
	 */
	public String getBtMac() {
		return this.neighbourProperties.get("btMacAddress").getPropertyValue();
	}

}
