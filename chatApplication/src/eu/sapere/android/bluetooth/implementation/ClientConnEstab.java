package eu.sapere.android.bluetooth.implementation;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

/**
 * This class manages the opened client/server BT connection between Sapere
 * devices. The handshaking sees the exchange of 4 parameters: <li>WIFI IP <li>
 * BT MAC ADDRESS <li>Node Neighbour Name <li>Node Neighbour Type
 * <p>
 * Received information is used to populate AddressDB.
 * 
 * @author Alberto Rosi
 * 
 */
public class ClientConnEstab extends Thread {

	private static final String TAG = "App_Client_ConnEstablished";
	private BluetoothSocket socket = null;
	String my_ip, ip;
	String my_mac_address;
	String mac_address;
	DataInputStream in;
	DataOutputStream out;
	Object monitor;
	AddressDB ab;
	String neighbourName, neighbourType, my_neighbourName, my_neighbourType;

	/**
	 * The ClientConnEstab constructor retrieves the parameters necessary for
	 * devices handshaking.
	 * 
	 * @param socket
	 *            the reference to server BT socket
	 * @param monitor
	 *            the reference to Object monitor
	 * @param ab
	 *            the reference to application AddressDB
	 * @param my_mac_address
	 *            local device BT mac address
	 * @param mac_address
	 *            remote device mac address
	 * @param my_neighbourName
	 *            local device neighbourName
	 * @param my_neighbourType
	 *            local device neighbourType
	 */
	public ClientConnEstab(BluetoothSocket socket, Object monitor,
			AddressDB ab, String my_mac_address, String mac_address,
			String my_neighbourName, String my_neighbourType) {
		this.socket = socket;
		this.monitor = monitor;
		this.my_mac_address = my_mac_address;
		this.mac_address = mac_address;
		this.ab = ab;
		this.my_neighbourName = my_neighbourName;
		this.my_neighbourType = my_neighbourType;
		my_ip = getLocalIpAddress();
	}

	/**
	 * Realizes the handshaking between devices.
	 */
	public void run() {
		try {
			synchronized (monitor) {

				Log.d(TAG, "-->Connection Accepted_Client: \n");

				out = new DataOutputStream(socket.getOutputStream());
				in = new DataInputStream(socket.getInputStream());

				ip = in.readUTF();
				neighbourName = in.readUTF();
				neighbourType = in.readUTF();

				ab.processIp(mac_address, ip, neighbourName, neighbourType);
				Log.d(TAG, "-->Network token recv: " + ip + " , " + mac_address
						+ " " + neighbourName + " " + neighbourType + " \n");

				out.writeUTF(my_ip);
				out.writeUTF(my_neighbourName);
				out.writeUTF(my_neighbourType);
				out.writeUTF(my_mac_address);

				socket.close();
				Log.d(TAG, "-->Connection terminated");
				monitor.notifyAll();
			}

		} catch (Exception e) {
			synchronized (monitor) {
				Log.e(TAG, e.toString());
				if (socket != null)
					try {
						socket.close();
					} catch (IOException f) {
						// TODO Auto-generated catch block
						Log.e(TAG, f.getMessage());
					}
				Log.e(TAG, e.getMessage());
				monitor.notifyAll();
			}
		}
	}

	/**
	 * Retrieves device WIFI IP address.
	 * 
	 * @return device IP
	 */
	public String getLocalIpAddress() {
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface
					.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf
						.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()) {
						return inetAddress.getHostAddress();
					}
				}
			}
		} catch (SocketException ex) {
			return "Not connected";
		}
		return "Not connected";
	}

}
