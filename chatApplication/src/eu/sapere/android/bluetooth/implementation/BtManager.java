/*
 * Copyright (C) 2011 Wglxy.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.sapere.android.bluetooth.implementation;

import java.util.Iterator;
import java.util.concurrent.ConcurrentMap;

import eu.sapere.android.bluetooth.BluetoothOverlayAnalyzer;
import eu.sapere.android.bluetooth.implementation.AddressDB.Record;

import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.LinearLayout;

/**
 * This is the IntentService managing Android OS api to device Bluetooth (BT)
 * Adapater. Actually Sapere MW implements two basic functionalities over BT
 * stacks: <li>ServerSocket connectivity: {@link BtServerService}
 * <p>
 * <li>Radar service: {@link BtRadarService}
 * <p>
 * ServerSocket connectivity opens one BT socket at time (this is an hardware
 * limitation) accepting BT client connections coming from other Sapere devices
 * for the mutual exchange of devices' WIFI IP.
 * 
 * <p>
 * Radar service measures the distance of BT-discoverable devices in terms of
 * RSSI dB. Actually the Radar service is called after each new ServerSocket
 * connection terminates. At the end of each Radar service routing the
 * PhysicalNetworkAnalyzer is notified of devices been detected.
 * 
 * A third functionality for opening ClientSocket connections has been developed
 * but actually not yet integrated.
 * 
 * @author Alberto Rosi
 */

public class BtManager extends IntentService {

	LinearLayout layout;
	boolean running_as_server = false;
	boolean running_as_client = false;
	boolean discovering_devices = false;
	private static final String TAG = "App_BTDiagnostic";
	private static Object monitor = new Object();
	private static BroadcastReceiver receiver;

	Intent btServerService, btClientService, btRadarService, BTManagerIntent;
	IntentFilter filter;
	BluetoothAdapter mBluetoothAdapter;
	AddressDB ab;

	private Context context;

	/**
	 * Basic empty IntentService constructor calling onCreate() routine.
	 */
	public BtManager() {
		super("BTManagerIntentService");

	}

	/**
	 * Enables BT and sets the device visible.
	 */

	public void onCreate() {

		super.onCreate();
		context = getApplicationContext();

		/**
		 * Enables device BT if not
		 */
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (!mBluetoothAdapter.isEnabled()) {
			mBluetoothAdapter.enable();
		}

		/**
		 * Set device visible if not. From kernel 2.3.6 visibility lasts
		 * forever. For previous kernel visibility is limited to 120 seconds.
		 */

		if (mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			BTManagerIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			BTManagerIntent.putExtra(
					BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
			BTManagerIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(BTManagerIntent);
		}

	}

	/**
	 * On startService() the ServerSocket service is called Intent remains alive
	 * since onDestroy() method is called from MiddlewareIntent to stop the
	 * whole application.
	 */
	@Override
	protected void onHandleIntent(Intent intent) {

		// Enable bluetooth if not
		filter = new IntentFilter();
		filter.addAction(BTResponseReceiver.FINISH_RESP_SERVER);
		filter.addAction(BTResponseReceiver.FINISH_RESP_CLIENT);
		filter.addAction(BTResponseReceiver.FINISH_RESP_RADAR);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		receiver = new BTResponseReceiver();

		registerReceiver(receiver, filter);

		btServerService = new Intent(context, BtServerService.class);
		btRadarService = new Intent(context, BtRadarService.class);
		btClientService = new Intent(context, BtClientService.class);

		start_server();

		synchronized (monitor) {
			try {
				monitor.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block

			}
		}

		stop_all();
		Log.d(TAG, "Stop all call!");

	}

	private void start_radar() {
		if (discovering_devices == false) {
			context.startService(btRadarService);
			Log.d(TAG, "Radar Application started!");
			discovering_devices = true;
		} else {
			// Toast.makeText(this, "Radar Application already started!",
			// Toast.LENGTH_LONG).show();
		}
	}

	private void start_server() {
		if (running_as_server == false) {
			Log.d(TAG, "Server Application started!");
			context.startService(btServerService);
			running_as_server = true;
		} else {
			// Toast.makeText(this, "Server Application already started!",
			// Toast.LENGTH_LONG).show();
		}
	}

	@SuppressWarnings("unused")
	private void start_client() {
		if (running_as_client == false) {
			Log.d(TAG, "Client Application started!");
			context.startService(btClientService);
			running_as_client = true;
		} else {
			// Toast.makeText(this, "Client Application already started!",
			// Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Before closing BTManger IntentService, each sub intent service must be
	 * stopped.
	 */

	private void stop_all() {

		if (running_as_server) {
			context.stopService(btServerService);
			running_as_server = false;
			Log.d(TAG, "BT server service stopped!");
		}
		if (discovering_devices) {
			mBluetoothAdapter.cancelDiscovery();
			discovering_devices = false;
		}
		if (running_as_client) {
			context.stopService(btClientService);
			running_as_client = false;
		}
	}

	/**
	 * Closes each active BT connections.
	 */
	public void onDestroy() {

		synchronized (monitor) {
			monitor.notifyAll();
		}
		super.onDestroy();
		unregisterReceiver(receiver);

	}

	/**
	 * The BTResponseReceiver class is in charge of perpetuating BT services
	 * over time, restarting each BT service (ServerSocket, ClientSocket, Radar)
	 * each time their Intent terminates. This comes from a limitation in
	 * Android where Bluetooh Services are intended to be deployed as one-shot.
	 * <p>
	 * Service reiteration is performed over three possible MW states:
	 * <li>running_as_server: if MW is set to accept BT connections (default)
	 * <p>
	 * <li>discovering_devices: if MW is performing the Radar service
	 * <p>
	 * <li>running_as_client: if MW is set to opening BT connections
	 * (functionality not already integrated)
	 * 
	 * @author Alberto Rosi
	 * 
	 */
	public class BTResponseReceiver extends BroadcastReceiver {

		static final String BT_RESP = "eu.sapere.intent.action.MESSAGE_BT_PROCESSED";
		static final String FINISH_RESP_RADAR = "eu.sapere.intent.action.RADAR_TERMINATE";
		static final String FINISH_RESP_SERVER = "eu.sapere.intent.action.SERVER_TERMINATE";
		static final String FINISH_RESP_CLIENT = "eu.sapere.intent.action.CLIENT_TERMINATE";

		AddressDB ab = ((AddressDB) getApplicationContext());

		/**
		 * Receives events from BT services ({@link BtServerService},
		 * {@link BtRadarService},{@link BtClientService}). Depending on BT
		 * state (running_as_server, discovering_devices, running_as_client) it
		 * restarts/closes services or notifies to PhysicalNetworkAnalyzer that
		 * a BT devices has been detected.
		 */
		public void onReceive(Context context, Intent intent) {

			/**
			 * Re launches both the ServerSocket service and Radar service every
			 * time one instance of ServerSocket has been terminated and the MW
			 * is still in running_as_server state
			 */

			if (intent.getAction()
					.equals(BTResponseReceiver.FINISH_RESP_SERVER)) {
				if (running_as_server) {
					context.stopService(btServerService);
					Log.d(TAG, "Starting Server! ");
					start_radar();
					context.startService(btServerService);
				}
			}

			if (intent.getAction()
					.equals(BTResponseReceiver.FINISH_RESP_CLIENT)) {
				running_as_client = false;
				Log.d(TAG, "Client application finished!");
			}

			/**
			 * Once the Radar Service terminates it notifies
			 * PhysicalNetworkAnalyzer for detected devices
			 */

			if (intent.getAction().equals(BTResponseReceiver.FINISH_RESP_RADAR)) {
				discovering_devices = false;
				Log.d(TAG, "Radar application finished!");
				devices_connectedNotifier();

			}

		}

		/**
		 * Sends to PhysicalNetworkAnalyzer the AddressDB content excluding
		 * not-Sapere devices, aka devices that haven't provide their IP.
		 */

		private void devices_connectedNotifier() {
			ConcurrentMap<String, Record> records = ab.getRecords();
			Log.d(TAG, "BT devices found: " + records.size() + "\n");
			Iterator<String> it1 = records.keySet().iterator();
			while (it1.hasNext()) {
				String mac_address = it1.next();
				Record value = records.get(mac_address);

				String ip = null;
				ip = value.getIp();
				if (ip != null) {

					BluetoothOverlayAnalyzer.onBtDeviceDetected(mac_address,
							value.getIp(), "" + value.getRssi(),
							value.getNeighbourName(), value.getNeighbourType());

					Log.d(TAG, "-->Mac Address: " + mac_address + "\t Rssi: "
							+ value.getRssi() + "\t Ip: " + value.getIp()
							+ "\t N name" + value.getNeighbourName() + "\n");
				}

			}
		}

	}

}

// end class
