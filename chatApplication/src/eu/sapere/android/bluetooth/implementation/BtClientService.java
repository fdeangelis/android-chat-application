package eu.sapere.android.bluetooth.implementation;

import java.io.IOException;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.ConcurrentMap;

import eu.sapere.android.bluetooth.implementation.AddressDB.Record;
import eu.sapere.android.bluetooth.implementation.BtManager.BTResponseReceiver;
import eu.sapere.middleware.node.NodeManager;

import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;

/**
 * The BtClientService is an IntentService managing Bluetooth client Socket
 * connectivity. Given device adapter limitations, it opens and manages only one
 * BT connection at time.
 * <p>
 * Once the connection is established, the {@link ClientConnEstab} class makes
 * Sapere devices to exchange middleware parameters.
 * <p>
 * Once the connection terminates the BtClientService goes in Destroy state; the
 * notifyEnd() method notifies the {@link BtManager} of such event.
 * 
 * @author Alberto Rosi
 */

public class BtClientService extends IntentService {

	static Intent broadcastIntent = new Intent();
	private static final String TAG = "App_BtClientService";
	final static String PARAM_OUT_MSG = "omsg";
	final static UUID SapereUUID = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");
	static BtClientService myinstance = null;

	Object monitor = new Object();
	BluetoothSocket socket = null;
	BluetoothDevice device = null;

	AddressDB ab;
	String space_name;
	String space_type;

	boolean started;

	/**
	 * Basic empty IntentService constructor calling onCreate() routine.
	 */
	public BtClientService() {
		super("BtClientService");
	}

	/**
	 * Retrieves parameters used in {@link ClientConnEstab} class.
	 */
	public void onCreate() {
		super.onCreate();

		ab = ((AddressDB) getApplicationContext());
		myinstance = this;

		// Check for SpaceName() not null, important in testing phase
		if (NodeManager.instance() != null)
			space_name = NodeManager.getSpaceName();
		else
			space_name = "space_name";

		space_type = "infrastructural_node";
	}

	/**
	 * On startService() the BtClientService for each BT mac address in
	 * AddressDB opens a Bluetooth socket connection. Once a connection is
	 * established a new Thread based on the {@link ClientConnEstab} class
	 * starts. The BtClientService intent terminates once all the devices in
	 * AddressDB have been tested for connection.
	 */
	protected void onHandleIntent(Intent intent) {

		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
				.getDefaultAdapter();
		ab = ((AddressDB) getApplicationContext());

		mBluetoothAdapter.cancelDiscovery();

		String my_mac_address = mBluetoothAdapter.getAddress();

		if (ab.getLength() > 0) {

			ConcurrentMap<String, Record> records = ab.getRecords();
			Iterator<String> it1 = records.keySet().iterator();
			while (it1.hasNext()) {
				String remote_mac_address = it1.next();
				boolean connection = false;
				try {
					device = mBluetoothAdapter
							.getRemoteDevice(remote_mac_address);

					Log.d(TAG,
							"--> Opening new connection to:"
									+ device.getAddress());
					socket = device
							.createRfcommSocketToServiceRecord(SapereUUID);
					socket.connect();
					connection = true;

					if (connection) {
						synchronized (monitor) {
							Log.d(TAG,
									"--> Connection accepted from:"
											+ device.getAddress());
							ClientConnEstab thread = new ClientConnEstab(
									socket, monitor, ab, my_mac_address,
									remote_mac_address, space_name, space_type);
							thread.start();
							try {
								monitor.wait(10000);
							} catch (InterruptedException e) {
								Log.e(TAG, e.getMessage());
								if (socket != null)
									try {
										socket.close();
									} catch (IOException f) {
										Log.e(TAG, f.getMessage());
									}
							}

						}
					}
				} catch (IOException e) {
					if (socket != null)
						try {
							socket.close();
						} catch (IOException f) {
							Log.e(TAG, f.getMessage());
						}
					Log.e(TAG, e.getMessage());
				}
			}
		} else {
			// sendMsg("AddressBook empty! Lanch the Radar to find devices");
		}

		notifyEnd();
	}

	private static BtClientService getInstance() {
		return myinstance;
	}

	/**
	 * Closes each active BT connections.
	 */
	public void onDestroy() {

		if (socket != null)
			try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.e(TAG, e.getMessage());
			}

	}

	/**
	 * Notify {@link BtManager} that the current instance of BtClientService has
	 * terminated.
	 */
	public static void notifyEnd() {

		Log.d(TAG, "Client notify end");
		broadcastIntent.setAction(BTResponseReceiver.FINISH_RESP_SERVER);
		broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
		getInstance().sendBroadcast(broadcastIntent);

	}

	/**
	 * Send a String to classes implementing the ResponseReceiver broadcaster.
	 * Typically the test GUI activity.
	 * 
	 * @param string
	 *            the string to be displayed.
	 */
	/*
	 * public static void sendMsg(String string) {
	 * 
	 * Log.d(TAG, "Server send back msg");
	 * 
	 * broadcastIntent.setAction(ResponseReceiver.ACTION_RESP);
	 * broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
	 * broadcastIntent.putExtra(PARAM_OUT_MSG, string);
	 * getInstance().sendBroadcast(broadcastIntent);
	 * 
	 * }
	 */

}
