package eu.sapere.android.bluetooth;

import android.util.Log;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.networking.topology.NetworkTopologyManager;
import eu.sapere.middleware.node.networking.topology.overlay.OverlayAnalyzer;
import eu.sapere.middleware.node.networking.topology.overlay.OverlayListener;

/**
 * The NetworkAnalyzer should provide the implementation to manage a bluetooth
 * overlay network.
 * 
 * @author Gabriella Castelli (UNIMORE)
 * @author Alberto Rosi (UNIMORE)
 */
public class BluetoothOverlayAnalyzer extends OverlayAnalyzer {

	private static String overlayname;
	
	private static final String TAG = "App_BTDiagnostic";

	/**
	 * Instantiates a NetworkAnalyzer.
	 * 
	 * @param listener
	 *            The NetworkListener associated to this NetworkAnalyzer.
	 * @param overlayName
	 *            The name of the overlay network
	 * 
	 */
	public BluetoothOverlayAnalyzer(OverlayListener listener, String overlayName) {

		super(listener, overlayName);
		overlayname = overlayName;
	}

	@Override
	public void run() {
		while (true) {
		}
	}

	public static void onBtDeviceDetected(String btMac, String ip, String rssi,
			String nodeName, String nodeType) {
		
		Log.d(TAG, "device detected "+ip);

		BluetoothNeighbour neighbour = new BluetoothNeighbour(btMac, ip, rssi,
				nodeName, nodeType);
		NetworkTopologyManager ntm = null;
		ntm = NodeManager.instance().getNetworkTopologyManager();
		if (ntm != null)
			ntm.onNeighbourFound(neighbour, overlayname);
	}

	@Override
	public void stopOverlay() {
		// TODO Auto-generated method stub

	}

}
