package eu.sapere.android;

import eu.sapere.demoChat.R;
import eu.sapere.android.SapereConsoleActivity.ConsoleLsaReceiver;
import eu.sapere.android.bluetooth.implementation.BtManager;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.lsaspace.console.LsaSpaceConsole;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Manages intents for the Sapere Middleware.
 * 
 * @author Gabriella Castelli (UNIMORE)
 * @author Alberto Rosi (UNIMORE)
 */
public class SapereMiddlewareIntent extends IntentService implements LsaSpaceConsole {

	//public static final String PARAM_IN_MSG = "imsg";
	public static final String PARAM_OUT_MSG = "omsg";
	Object monitor = new Object();
	//public static final String PIC_URL = "sapere.intent.action.URL_RECEIVED";

	private static Context context;

	private static SapereMiddlewareIntent myinstance;
	Intent BTManager;

	NodeManager sm;
	boolean running;

	public SapereMiddlewareIntent() {
		super("SapereMiddlewareIntent");

	}

	@Override
	public void onCreate() {
		super.onCreate();
		context = getApplicationContext();

	}
  
	public int onStartCommand(Intent intent, int flags, int startId) {
		myinstance = this;
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	protected void onHandleIntent(Intent arg0) {

		BTManager = new Intent(context, BtManager.class);
		context.startService(BTManager);
		Log.d("Intent", "BtManager started!");
		sm = NodeManager.instance(myinstance);

		synchronized (monitor) {
			try {
				monitor.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block

			}
		}
 
		sm.stopServices();
		context.stopService(BTManager);
		Log.d("Intent", "MW intent shut down!");
 
	}

	public static Context getContext() {
		return context;
	}

	public static LsaSpaceConsole getInstance() {
		return myinstance;
	}

	public void sendMsg(String s[]) {

		Log.d("Intent", s[0].toString());

		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction(ConsoleLsaReceiver.ACTION_RESP);
		broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
		broadcastIntent.putExtra(PARAM_OUT_MSG, s);
		sendBroadcast(broadcastIntent);

	}

	@Override
	public void update(Lsa[] list) {

		String result[] = new String[list.length];
		for (int i = 0; i < list.length; i++) {
			result[i] = list[i].toString();
		}

		sendMsg(result);
	}

	public void updateURL(String s) {
		Log.d("Intent", "Url sent");
	}

	public void onDestroy() {

		synchronized (monitor) {
			monitor.notifyAll();
		}
		super.onDestroy();
	}

	@Override
	public void removeLsa(String lsaId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeBond(String from, String to) {
		// TODO Auto-generated method stub

	}

	@Override
	public void propagateLsa(String lsaId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addBidirectionalBond(String from, String to) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addUnidirectionalBond(String from, String to) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean hasInspector() {
		// TODO Auto-generated method stub
		return false;
	}

}
