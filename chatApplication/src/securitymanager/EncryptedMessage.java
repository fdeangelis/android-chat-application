/*
 * The encrypted message sent across the network.
 * 
 * GPL v.3
 * 
 * Francesco De Angelis
 * 
 * francesco.deangelis@unige.ch
 * 
 * 
 */
package securitymanager;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import tools.Util;

public class EncryptedMessage 
{
	public byte [] body;
	public byte [] iv;
	public byte [] sessionKey;
	public String serError;
	
	public EncryptedMessage()
	{
	}
	
	public EncryptedMessage(byte[] body, byte [] iv, byte [] sessionKey)
	{
		this.body = body;
		this.iv = iv;
		this.sessionKey = sessionKey;
	}
	
	public String encoded()
	{
        byte [][] multi = new byte[3][];
        multi[0] = body;
        multi[1] = iv;
        multi[2] = sessionKey;
        
		// new byte stream
		ByteArrayOutputStream bs = new ByteArrayOutputStream();
        // serialize the content of the message
        try
        {
    			// serializer
            ObjectOutputStream sr = new ObjectOutputStream(bs);
	        sr.writeObject(multi);
	        sr.close();
	        return Util.encode(bs.toByteArray());	
        }
        catch(Exception ex)
        {
        		return ex.getMessage();
        }
	}
	
	public static EncryptedMessage decode(String encoded)
	{
		EncryptedMessage mex = null;
		// decode the EncryptedMessage
		//
		try
		{
			ByteArrayInputStream bs = new ByteArrayInputStream(Util.decode(encoded)); 
			ObjectInputStream iis = new ObjectInputStream(bs);
			byte [][] content = (byte[][]) iis.readObject();
			mex = new EncryptedMessage(content[0], content[1],content[2]);
			return mex;
		}
		catch(Exception ex)
		{
			mex = new EncryptedMessage();
			mex.serError = ex.getMessage();
			return mex;
		}
	}
}
